#+LaTeX_HEADER: \usepackage[inkscapelatex = false, inkscapearea = page]{svg}
#+LaTeX_HEADER: \usepackage{array, multirow, makecell, hhline, float}
#+LaTeX_HEADER: \usepackage[linesnumbered, ruled, vlined]{algorithm2e}
#+LaTeX_HEADER: \SetKwComment{Comment}{$\triangleright$\ }{}
#+LaTeX_HEADER: \SetCommentSty{}
#+LaTeX_HEADER: \usepackage{xcolor, mathtools, tikz, caption, subcaption}
#+LaTeX_HEADER: \definecolor{brokensymmetry}{RGB}{0, 128, 128}
#+LaTeX_HEADER: \definecolor{density}{RGB}{255, 102, 0}
#+LaTeX_HEADER: \definecolor{repetition}{RGB}{128, 0, 128}
#+LaTeX_HEADER: \definecolor{multisolve}{RGB}{137, 204, 202}
#+LaTeX_HEADER: \definecolor{multifacto}{RGB}{101, 97, 169}
#+LaTeX_HEADER: \usetikzlibrary{matrix, positioning, decorations.pathreplacing}
#+LaTeX_HEADER: \usepackage{styles/kbordermatrix}
