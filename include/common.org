#+AUTHOR: Emmanuel Agullo, Marek Felšöci, Amina Guermouche, Hervé Mathieu,
#+AUTHOR: Guillaume Sylvand, Bastien Tagliaro
#+DATE: {{{time(%B %d\, %Y)}}}
#+STARTUP: showeverything
#+OPTIONS: timestamp:nil
