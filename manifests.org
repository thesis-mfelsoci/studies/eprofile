* Environment specification
:PROPERTIES:
:CUSTOM_ID: manifest
:END:

To enter a particular software environment using Guix, we use the =guix shell=
command. Options of the latter allows us to specify the packages to include
together with the desired version, commit or branch.

To avoid typing lot of command line options anytime we want to enter the
environment, we use a Guix manifest file cite:guix-manifests describing these
options in the Scheme language.

We use two different manifest files, one for performing experiments and one for
post-processing results and publishing documents.

* Performing experiments
:PROPERTIES:
:CUSTOM_ID: performing-experiments
:header-args: :tangle experiments.scm :padline no
:END:

We first describe any transformations that are supposed to be made to the
packages in environment.

#+BEGIN_SRC scheme
(use-modules (guix transformations))
#+END_SRC

In this case, we need to:

- specify an alternative source of {{{gcvb}}} accessible through the proxy
  server of the PlaFRIM computing platform as well as a specific revision of the
  package allowing for running benchmarks within Singularity containers,
  #+BEGIN_SRC scheme
(define transform1
  (options->transformation
    '((with-git-url
       .
       "gcvb-minimal-felsocim=https://gitlab.inria.fr/mfelsoci/gcvb")
      (with-commit
       .
       "gcvb-minimal-felsocim=9adbba1c6e486b9ec52e96a2eb697e64332be278")
  #+END_SRC
- replace OpenBLAS by Intel(R) MKL in the dependency tree.
  #+BEGIN_SRC scheme
      (with-input . "mumps-scotch-openmpi=mumps-mkl-scotch-openmpi")
      (with-input . "pastix-5=pastix-5-mkl")
      (with-input . "openblas=mkl"))))
  #+END_SRC

Finally, we list all of the packages to include into the environment and apply
the transformation to them.

#+BEGIN_SRC scheme
(packages->manifest
  (list (transform1 (specification->package "scab-energy_scope"))
        (transform1 (specification->package "openmpi"))
        (transform1 (specification->package "gcvb-minimal-felsocim"))
        (transform1 (specification->package "python-psutil"))
        (transform1 (specification->package "python-numpy"))
        (transform1 (specification->package "python-matplotlib"))
        (transform1 (specification->package "python@3"))
        (transform1 (specification->package "gzip"))
        (transform1 (specification->package "tar"))
        (transform1 (specification->package "openssh"))
        (transform1 (specification->package "bc"))
        (transform1 (specification->package "jq"))
        (transform1 (specification->package "likwid"))
        (transform1 (specification->package "sed"))
        (transform1 (specification->package "which"))
        (transform1 (specification->package "grep"))
        (transform1 (specification->package "findutils"))
        (transform1 (specification->package "inetutils"))
        (transform1 (specification->package "coreutils"))
        (transform1 (specification->package "bash"))))
#+END_SRC

* Post-processing and publishing
:PROPERTIES:
:CUSTOM_ID: post-processing-and-publishing
:header-args: :tangle ppp.scm :padline no
:END:

There is only one package tranformation to be applied in this manifest. We need
to use a more recent version of the ~ggplot~ package than the one that is
defined in the mainstream Guix.

#+BEGIN_SRC scheme
(use-modules (guix transformations))

(define transform1
  (options->transformation
    '((with-input . "r-ggplot2=r-ggplot2@git.bd50a551"))))
#+END_SRC

The list of packages to include into the environment follows.

#+BEGIN_SRC scheme
(packages->manifest
  (list (transform1 (specification->package "python-pygments"))
        (transform1 (specification->package "python@3"))
        (transform1 (specification->package "texlive"))
        (transform1 (specification->package "r"))
        (transform1 (specification->package "r-dbi"))
        (transform1 (specification->package "r-rsqlite"))
        (transform1 (specification->package "r-plyr"))
        (transform1 (specification->package "r-dplyr"))
        (transform1 (specification->package "r-readr"))
        (transform1 (specification->package "r-tidyr"))
        (transform1 (specification->package "r-ggplot2@git.bd50a551"))
        (transform1 (specification->package "r-scales"))
        (transform1 (specification->package "r-cowplot"))
        (transform1 (specification->package "r-stringr"))
        (transform1 (specification->package "r-gridextra"))
        (transform1 (specification->package "r-ggrepel"))
        (transform1 (specification->package "r-rjson"))
        (transform1 (specification->package "r-ascii"))
        (transform1 (specification->package "r-r-utils"))
        (transform1 (specification->package "inkscape"))
        (transform1 (specification->package "svgfix"))
        (transform1 (specification->package "graphviz"))
        (transform1 (specification->package "emacs"))
        (transform1 (specification->package "emacs-org2web"))
        (transform1 (specification->package "emacs-org"))
        (transform1 (specification->package "emacs-htmlize"))
        (transform1 (specification->package "emacs-biblio"))
        (transform1 (specification->package "emacs-org-ref"))
        (transform1 (specification->package "emacs-ess"))
        (transform1 (specification->package "sed"))
        (transform1 (specification->package "which"))
        (transform1 (specification->package "coreutils"))
        (transform1 (specification->package "bash"))))
#+END_SRC
