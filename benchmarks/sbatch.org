* {{{sbatch}}} template files
:PROPERTIES:
:CUSTOM_ID: sbatch-templates
:END:

We use {{{slurm}}} cite:slurm to schedule and execute our experiments on the
target high-performance computing platforms. {{{gcvb}}} produces a job script
for each benchmark described in the definition file. This script is then passed
to {{{slurm}}} for scheduling on a computation node or nodes.

Each job script produced by {{{gcvb}}} is prepended with a header contaning the
configuration statements for the {{{sbatch}}} command of {{{slurm}}}
cite:slurmGuide used to submit jobs for computation. We take advantage of the
template feature in order to be able to dynamically generate =#SBATCH= headers
specific to a given set of benchmarks.

An {{{sbatch}}} template begins as a standard shell script.

#+NAME: sbatch-beginning
#+BEGIN_SRC shell
#! /usr/bin/env bash
#
# slurm batch job script
#
#+END_SRC

We use multiple template files but most of the =#SBATCH= directives are common
to all of them:

- count of computational nodes to reserve,
  #+HEADER: :noweb-ref sbatch-end
  #+BEGIN_SRC shell
#SBATCH -N {scheduler[nodes]}
  #+END_SRC
- count of task slots per node to reserve,
  #+HEADER: :noweb-ref sbatch-end
  #+BEGIN_SRC shell
#SBATCH -n {scheduler[tasks]}
  #+END_SRC
- exclusion of the other users from the usage of the reserved resources,
  #+HEADER: :noweb-ref sbatch-end
  #+BEGIN_SRC shell
#SBATCH --exclusive
  #+END_SRC
- reservation time,
  #+HEADER: :noweb-ref sbatch-end
  #+BEGIN_SRC shell
#SBATCH --time={scheduler[time]}
  #+END_SRC
- location to place the slurm log files in where =%x= is the corresponding job
  name and =%j= the identifier of the job,
  #+HEADER: :noweb-ref sbatch-end
  #+BEGIN_SRC shell
#SBATCH -o slurm/%x-%j.log
  #+END_SRC
- we exclude selected nodes because of inconsistent configuration (less RAM than
  expected).
  #+HEADER: :noweb-ref sbatch-end
  #+BEGIN_SRC shell
#SBATCH --exclude=miriel019,miriel023,miriel030,miriel056
  #+END_SRC

Note that ={scheduler[nodes]}= and so on represent placeholders for values
replaced by actual values based on the definition Yaml file during template
expansion (see Section [[#definition-file]]).

One of the =#SBATCH= directives specific to each template file is the job name.
Based on the job name, we distinguish different sets of benchmarks. Grouping
individual benchmarks into a single job script allows us to submit less jobs.
This way, they are more balanced in terms of the computation time we need to
reserve for them on the target computing platform. For example, instead of
submitting 12 jobs having each the time limit of 10 minutes, we submit two jobs
with the time limit of 1 hour each. Benchmarks to be placed into a given common
job script are identified by matching their job name against a regular
expression. Finally, the =--constraint= switch allows us to specify the node
family to rely on.

At the end of the header, we:

- disable the creation of memory dump files in case of a memory error (useful
  but taking too much disk space and preventing other jobs from running),
  #+HEADER: :noweb-ref sbatch-end
  #+BEGIN_SRC shell
ulimit -c 0
  #+END_SRC

- record the date and the time when the job was scheduled and on which node.
  #+HEADER: :noweb-ref sbatch-end
  #+BEGIN_SRC shell
echo "Job scheduled on $(hostname), on $(date)."
echo
  #+END_SRC

** ~sbatch-overhead~
:PROPERTIES:
:CUSTOM_ID: sbatch-overhead
:header-args: :tangle sbatch-overhead :noweb yes
:END:

The {{{sbatch}}} header template ~sbatch-overhead~ is meant for benchmarks
measuring overhead of the performance monitoring tools we use.

#+BEGIN_SRC shell
<<sbatch-beginning>>
#SBATCH --job-name={scheduler[prefix]}-{scheme[label]}
#SBATCH --constraint={scheduler[family]}
<<sbatch-end>>
#+END_SRC

** ~sbatch-simple~
:PROPERTIES:
:CUSTOM_ID: sbatch-simple
:header-args: :tangle sbatch-simple :noweb yes
:END:

The {{{sbatch}}} header template ~sbatch-simple~ is for single-node
{{{test_FEMBEM}}} benchmarks meant to be executed all on the same node.

#+BEGIN_SRC shell
<<sbatch-beginning>>
#SBATCH --job-name={scheduler[prefix]}
#SBATCH --constraint={scheduler[family]}
<<sbatch-end>>
#+END_SRC

** ~sbatch-single-node~
:PROPERTIES:
:CUSTOM_ID: sbatch-single-node
:header-args: :tangle sbatch-single-node :noweb yes
:END:

The {{{sbatch}}} header template ~sbatch-single-node~ is for single-node
{{{test_FEMBEM}}} benchmarks on coupled sparse/dense FEM/BEM systems. Here, we
use one node per implementation scheme.

#+BEGIN_SRC shell
<<sbatch-beginning>>
#SBATCH --job-name={scheduler[prefix]}-{case[scheme]}
#SBATCH --constraint={scheduler[family]}
<<sbatch-end>>
#+END_SRC

** ~sbatch-multi-node~
:PROPERTIES:
:CUSTOM_ID: sbatch-multi-node
:header-args: :tangle sbatch-multi-node :noweb yes
:END:

The {{{sbatch}}} header template ~sbatch-multi-node~ is for multi-node
{{{test_FEMBEM}}} benchmarks on coupled sparse/dense FEM/BEM systems. Here, we
use one set of nodes per implementation scheme. The ={scheduler[constraint]}=
placeholder allow us to add an extra node selection constraint, e.g.
inter-connecting network type.

#+BEGIN_SRC shell
<<sbatch-beginning>>
#SBATCH --job-name={scheduler[prefix]}-{case[scheme]}
#SBATCH --constraint={scheduler[family]}{scheduler[constraint]}
<<sbatch-end>>
#+END_SRC
