* Configuration file
:PROPERTIES:
:CUSTOM_ID: configuration-file
:header-args: :tangle config.yaml :padline no
:END:

The configuration file is designed to provide a machine-specific information for
a {{{gcvb}}} benchmark collection such as the submit command for job scripts,
etc. Nevertheless, our configuration does not vary from machine to machine, so
we use the same ~config.yaml~ everywhere.

The configuration of a {{{gcvb}}} benchmark collection is simple. It usually
holds in a few lines of code beginning by a machine identifier.

#+BEGIN_SRC yaml
machine_id: generic
#+END_SRC

The most important is to define the path to the executable used to submit job
scripts produced by {{{gcvb}}}. We rely on slurm as workload manager and we use
its {{{sbatch}}} command to submit job scripts.

#+BEGIN_SRC yaml
submit_command: sbatch
#+END_SRC

Sometimes, we need to re-run the validation phase (see Section
[[#definition-file]]) of benchmarks without repeating the computation itself,
e.g. in case of a change in the result parsing script (see Section
[[#result-parsing]]). This phase does not have to be performed as a Slurm job on
a separate node. Therefore, we do not want to use {{{sbatch}}} as submit command
but simply execute the job script produced by {{{gcvb}}} in a Bash shell on the
current node.

#+begin_src yaml
va_submit_command: bash
#+end_src

Also, an associative list of executables can be defined for a handy access from
definition file. For now, we do not use this feature.

#+BEGIN_SRC yaml
executables: []
#+END_SRC

In order to prevent recompiling packages in Guix due to frequent garbage
collection in the store as well as proprietary source code exposure in the
latter, we sometimes prefer to execute benchmarks inside of a Singularity
container. To this end, we have to specify a Singularity command and its options
including the Singularity bundle to use.

#+BEGIN_SRC yaml
singularity: [
  "singularity", "exec", "~/containers/eprofile-experiments.gz.squashfs"
]
#+END_SRC

