# Study of the processor and memory power consumption of coupled sparse/dense solvers

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/studies/eprofile/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/studies/eprofile/-/commits/master)

In the aeronautical industry, aeroacoustics is used to model the propagation of
acoustic waves in air flows enveloping an aircraft in flight. This for instance
allows one to simulate the noise produced at ground level by an aircraft during
the takeoff and landing phases, in order to validate that the regulatory
environmental standards are met. Unlike most other complex physics simulations,
the method resorts to solving coupled sparse/dense systems. In a previous study,
we proposed two classes of algorithms for solving such large systems on a
relatively small workstation (one or a few multicore nodes) based on compression
techniques. The objective of this paper is to assess whether the positive impact
of the proposed algorithms on time to solution and memory usage translates to
the energy consumption as well. Because of the nature of the problem, coupling
dense and sparse matrices, and the underlying solutions methods, including
dense, sparse direct and compression steps, this yields an interesting processor
and memory power profile which we aim to analyze in details.

[**Explore**](https://thesis-mfelsoci.gitlabpages.inria.fr/studies/eprofile)

## Collaborator instructions

For the co-authors of the article associated with this study to be able to
constribute to the manuscript, we provide, in this section, the instructions
allowing one to reproduce the software environment required for creating the
output PDF document `article-sbac-pad.pdf`. We use the Guix transactional
package manager to handle software environments. Guidelines are provided both
for machines running Guix package manager natively and for machines without Guix
but running Singularity container solution.

In both cases, you first need to clone this repository.

```shell
git clone https://gitlab.inria.fr/thesis-mfelsoci/studies/eprofile.git
```

Then, navigate to the clone, download and extract the latest version of figures
featuring experimental results.

```shell
cd eprofile
wget -O figures.zip https://gitlab.inria.fr/thesis-mfelsoci/studies/eprofile/-/jobs/artifacts/master/download?job=figures
unzip figures.zip
```

Remaining instructions depend on your machine's configuration.

### Running Guix natively

If you are running Guix natively, you will need to tangle software environment
specification (channels and manifest) from the corresponding Org documents.

```shell
guix shell --pure git emacs emacs-org -- emacs --batch --no-init-file -l org --eval '(progn (setq org-src-preserve-indentation t) (dolist (file (directory-files-recursively "." "\\.org$")) (org-babel-tangle-file file)))'
```

Now, you can enter the target software environment and compile the Org source of
the article into a LaTeX-produced PDF file using the command below.

```shell
guix time-machine -C channels.scm -- shell --pure -m ppp.scm -- emacs --batch --no-init-file --load publish.el --eval '(org-publish "article-sbac-pad")'
```

### Using a Singularity bundle

If your machine does not run Guix natively, you can use a pre-built Singularity
bundle containing the required software environemnt available for download until
**July 14, 2022**.

```
wget -O eprofile-ppp.gz.squashfs https://filesender.renater.fr/?s=download&token=eaf9321f-3d8e-4432-bfdd-5b8cc295c0bb
```

To compile the Org source of the article into a LaTeX-produced PDF file using
Singularity and the dedicated bundle, you can use the command below. Note that
here we suppose that the Singularity image is present in the current working
directory.

```shell
singularity exec --cleanenv eprofile-ppp.gz.squashfs emacs --batch --no-init-file --load publish.el --eval '(org-publish "article-sbac-pad")'
```
