(require 'org) ;; Org mode support
(require 'htmlize) ;; source code block export to HTML
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions
(require 'org-ref) ;; bibliography support

;; Enable Babel code evaluation.
(setq org-export-babel-evaluate nil)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)))

;; Avoid overriding of default table caption placement by `styles/styles.el'.
(setq custom-caption-placement t)

;; Load style presets.
(load-file "styles/styles.el")

;; Configure HTML website publishing.
(setq org-publish-project-alist
      (list
       (list "environment"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["environment.org"]
             :preparation-function '(disable-babel)
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public")
       (list "article-sbac-pad"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["article-sbac-pad.org"]
             :preparation-function '(enable-babel)
             :publishing-function '(org-latex-publish-to-pdf-verbose)
             :publishing-directory "./public")
       (list "research-report"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["research-report.org"]
             :latex-caption-above nil
             :preparation-function '(enable-babel)
             :publishing-function '(org-html-publish-to-html
                                    org-latex-publish-to-pdf-verbose)
             :publishing-directory "./public")
       (list "results"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["eprofile.org" "research-report.org"]
             :preparation-function '(enable-babel)
             :completion-function '(fix-svg)
             :publishing-directory "."
             :publishing-function '(org-babel-execute-file))
       (list "figures"
             :base-directory "./figures"
             :base-extension "png\\|jpg\\|gif\\|svg\\|ico"
             :recursive t
             :publishing-directory "./public/figures"
             :publishing-function '(org-publish-attachment))
       (list "eprofile"
             :components '("environment" "article-sbac-pad" "research-report"))
       (list "eprofile-static"
             :components '("figures"))))

(provide 'publish)
