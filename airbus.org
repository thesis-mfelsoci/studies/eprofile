* Version specification of Airbus packages
:PROPERTIES:
:CUSTOM_ID: airbus-versions
:header-args: :tangle airbus.sh
:END:

In order to be able to acquire the source tarballs of Airbus packages using the
~getsrc.sh~ script as described in Section [[#proprietary-sources]], we define
here a shell script specifying the versions we need to acquire through a set of
environment variables.

#+BEGIN_SRC shell
HMAT_PACKAGE="hmat"
HMAT_VERSION="2021.1.0-21.52c4f6c"
HMAT_COMMIT="52c4f6c9dfa084ad67bb9876c30a049edbd82b07"
HMAT_BRANCH="master"
MPF_PACKAGE="mpf-energy_scope"
MPF_VERSION="2021.1.0-27.9aba318"
MPF_COMMIT="9aba3184bb806dc0a05e2a933bfe858472bb207a"
MPF_BRANCH="mf/energy_scope"
SCAB_PACKAGE="scab-energy_scope"
SCAB_VERSION="2021.1.0-32.3056f3c"
SCAB_COMMIT="3056f3c7a986cc2fabb6663707272ccf16fbb614"
SCAB_BRANCH="mf/energy_scope"
#+END_SRC
